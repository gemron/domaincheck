package com.gemron;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class WanWangDomain {

	public static void main(String[] args) {
		int domainLength = 2;
		String isHaveNum = "n";
		int tNum = 50;
		int tTime = 10;
		String path = "D:\\domain\\domains.txt";
		String startDomain = "";
		String domianEnd = ".com";
		if (args.length < 7) {
			System.out
					.println("请输入参数  域名位数 是否包含数字 线程数 线程间隔时间(毫秒)  域名存放位置 域名开始   后缀");
			System.out
					.println("比如：    2 y 50 10 d:\\domain\\domains.txt aa .com");
			System.out.println("不输入参数按以上默认值");

		} else {
			domainLength = Integer.valueOf(args[0]);
			isHaveNum = args[1];
			tNum = Integer.valueOf(args[2]);
			tTime = Integer.valueOf(args[3]);
			path = args[4];
			startDomain = args[5];
			if (startDomain.length() != domainLength) {
				System.out.println("域名开始 的长度请和定义长度保持一致");
				System.exit(0);
			}
			domianEnd = args[6];
		}
		List<String> domainList = domainGen.getDomainList(domainLength,
				isHaveNum);
		try {
			File f = new File(path);
			f.getParentFile().mkdirs();
			FileOutputStream fileOutputStream = new FileOutputStream(f, true);

			ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors
					.newFixedThreadPool(tNum);
			boolean isStart = false;
			for (int i = 0; i < domainList.size(); i++) {
				if (domainList.get(i).equals(startDomain)
						|| "".equals(startDomain)) {
					isStart = true;
				}
				if (isStart) {
					String domain = domainList.get(i) + domianEnd;
					DoJobThread doJobThread = new DoJobThread(i,
							fileOutputStream, domain);
					threadPoolExecutor.submit(doJobThread);
					Thread.sleep(tTime);
				}

			}

			while (true) {
				// System.out.println(threadPoolExecutor.getActiveCount());
				if (threadPoolExecutor.getActiveCount() == 0) {
					threadPoolExecutor.shutdown();
					fileOutputStream.flush();
					fileOutputStream.close();
					break;
				}

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String doHttp(String url, String... param) throws Exception {
		HttpGet httpGet = new HttpGet(url);

		// 设置编码
		try {
			HttpResponse response = new DefaultHttpClient().execute(httpGet);
			// 可以得到指定的header
			/*
			 * Header header = response.getFirstHeader("Content-Length"); String
			 * Length = header.getValue(); System.out.println(header.getName() +
			 * ":" + Length);
			 */
			// 如果状态码是200，则正常返回
			if (response.getStatusLine().getStatusCode() == 200) {
				// 获得返回的字符串
				String result = EntityUtils.toString(response.getEntity());
				// System.out.println(result);
				return result;
				// 打印输出

			} else {
				System.err.println(response.getStatusLine().getStatusCode());
			}
		} catch (Exception e) {
			throw e;
		}
		return "err";
	}

}

class DoJobThread implements Callable<String> {
	private FileOutputStream fileOutputStream;
	private String domain;
	private int i;
	private int tryNum = 3;

	public DoJobThread(int i, FileOutputStream fileOutputStream, String domain) {
		this.fileOutputStream = fileOutputStream;
		this.domain = domain;
		this.i = i;
	}

	@Override
	public String call() throws Exception {
		// System.out.println(Thread.currentThread().getName());
		try {

			String res = WanWangDomain
					.doHttp("http://panda.www.net.cn/cgi-bin/check.cgi?area_domain="
							+ domain);
			System.out.println(domain
					+ ":"
					+ (res.indexOf("Domain name is available") > 0 ? "ok"
							: "no ok"));
			if (res.indexOf("Domain name is available") > 0) {
				String save = domain + "\r\n";
				fileOutputStream.write(save.getBytes());
				// System.out.println(i + "--:" + domainList.get(i));
			} else if (res.indexOf("Timeout") > 0) {
				throw new Exception("timeout");
			}

		} catch (Exception e) {

			if (tryNum == 0) {
				return "err";
			}
			tryNum--;
			System.err.println("err:" + i + "--" + domain + "--"
					+ e.getMessage());
			Thread.sleep(1000 * 300);
			this.call();
		}
		return "ok";
	}

}
