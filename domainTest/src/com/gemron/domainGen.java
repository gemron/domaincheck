package com.gemron;

import java.util.ArrayList;
import java.util.List;

public class domainGen {
	public static void main(String[] args) {
		List<String> list = new ArrayList<String>();
		int length = 2;
		appendDomain(list, "", length, false);
		for (String string : list) {
			System.out.println(string);
		}
	}

	public static void appendDomain(List<String> b, String pre, int length,
			boolean haveNum) {
		int i = 48;
		if (!haveNum) {
			i = 97;
		}
		length--;
		for (; i < 123; i++) {
			String str = Character.toString((char) i);
			String res = pre + str;
			if (length == 0) {
				b.add(res);
			} else {
				appendDomain(b, res, length, haveNum);
			}

			if (i == 57) {
				i = 96;
			}

		}

	}

	public static List<String> getDomainList(int length, String isHaveNum) {
		boolean haveNum = "y".equals(isHaveNum.toLowerCase());
		List<String> list = new ArrayList<String>();
		appendDomain(list, "", length, haveNum);
		return list;
	}
}
