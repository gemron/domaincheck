package com.gemron;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Entry {

	public static void main(String[] args) {
		List<String> domainList = domainGen.getDomainList(4,"y");
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(
					"D:\\taoBaoDomain\\domains.txt", true);
			for (int i = 9589; i < domainList.size(); i++) {
				String res = doHttp(
						"http://ishop.taobao.com/subdomain/searchSubDomainResult.htm?_input_charset=utf-8",
						domainList.get(i));
				// System.out.println(res);
				if ("检索成功".equals(res)) {
					String save = i + "--:" + domainList.get(i)+"\r\n";
					fileOutputStream.write(save.getBytes());
					//System.out.println(i + "--:" + domainList.get(i));
				}
			}
			fileOutputStream.flush();
			fileOutputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static String doHttp(String url, String... param) {
		HttpPost httpPost = new HttpPost(url);

		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		// 添加参数
		String doamin = param[0];
		
		params.add(new BasicNameValuePair("_tb_token_", "0"));
		params.add(new BasicNameValuePair("domain", doamin));
		// 设置编码
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(params, "utf-8"));
			httpPost.setHeader(
					"cookie",
					"v=0; _tb_token_=7bde776be3675; tg=0; _cc_=VFC%2FuZ9ajQ%3D%3D; t=6a8f26616a4433c1a4ac5a60c1d891f8; unb=64829337; _nk_=guomengyue; _l_g_=Ug%3D%3D; cookie2=1bea8703e50db4982a48aceefdc6d80d; tracknick=guomengyue; sg=e7d; lastgetwwmsg=MTM3NDAzNTE5Mw%3D%3D; mt=np=; cookie1=UUGg%2FHDqY3K337ctF1rPwNMtojIi0peqtO9Xf%2Fn7cx0%3D; cookie17=VWsg7dCuBRQ%3D; tlut=UoLbu50TFkmcrw%3D%3D; uc1=lltime=1374030156&cookie14=UoLbu50TFkpatA%3D%3D&existShop=true&cookie16=Vq8l%2BKCLySLZMFWHxqs8fwqnEw%3D%3D&cookie21=W5iHLLyFfXVRDP8mxoRA8A%3D%3D&tag=0&cookie15=VT5L2FSpMGV7TQ%3D%3D");
			// 发送Post,并返回一个HttpResponse对象
			HttpResponse response = new DefaultHttpClient().execute(httpPost);
			// 可以得到指定的header
			/*
			 * Header header = response.getFirstHeader("Content-Length"); String
			 * Length = header.getValue(); System.out.println(header.getName() +
			 * ":" + Length);
			 */
			// 如果状态码是200，则正常返回
			if (response.getStatusLine().getStatusCode() == 200) {
				// 获得返回的字符串
				String result = EntityUtils.toString(response.getEntity());
				ObjectMapper mapper = new ObjectMapper();
				JsonNode jsonNode = mapper.readTree(result);
				String res = jsonNode.path("message").asText();
				return res;
				// 打印输出

			} else {
				System.err.println(response.getStatusLine().getStatusCode());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "err";
	}
}
